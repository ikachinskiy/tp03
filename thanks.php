<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Подписался - не удивляйся</title>
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/clearbg.css">
</head>
<body>
<div class="main">
    <header>
        <a href="index.php"><img class="logo" src="images/logo.jpg"></a>
        <ul id="main-menu">
            <li><a href="#">Об нас</a></li>
            <li><a href="#">Вступить</a></li>
            <li><a href="#">Команда</a></li>
        </ul>
        <ul id="user-menu">
            <li><a href="#">Войти</a></li>
            <li><a href="#">Регистрация</a></li>
        </ul>
        <form id="form-search" name="form-search" method="get" action="search.php">
            <input type="text" name="search-text" id="search-text">
            <input type="submit" value="Искать" id="search-submit">
        </form>
    </header>
    <section class="content">
    </section>
    <footer>
        <section id="fsect1">
            <h3>Об нас</h3>
            <p>Мы вольные каменщики</br>Строим новый Мир.</p>
            <ul id="social-menu">
                <li><a href="#">Fb</a></li>
                <li><a href="#">Ln</a></li>
                <li><a href="#">Tw</a></li>
            </ul>
        </section>
        <section id="fsect2">
            <h3>Наши сервисы</h3>
            <ul class="footer-menu">
                <li><a href="#">Цветные революции</a></li>
                <li><a href="#">Уборка мусора</a></li>
            </ul>
        </section>
        <section id="fsect3">
            <h3>Партнёры</h3>
            <ul class="footer-menu">
                <li><a href="#">Шурик</a></li>
                <li><a href="#">Юрик</a></li>
                <li><a href="#">Лёлик</a></li>
            </ul>
        </section>
        <section id="fsect4">
            <h3>Подпишитесь</h3>
            <form id="form-subscribe" name="form-subscribe" method="get" action="thanks.php">
                <input type="email" name="e-mail" id="e-mail" value="Ваш адрес">
                <input type="submit" value="Go" id="SubscrOk">
            </form>
            <p>© SmartNET 2016</p>
        </section>
    </footer>
</div>
</body>
</html>