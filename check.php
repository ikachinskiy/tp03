<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Социализм - это учёт и контроль</title>
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/clearbg.css">
    <link rel="stylesheet" href="css/check.css">
</head>
<body>
<div class="main">
    <header>
        <a href="index.php"><img class="logo" src="images/logo.jpg"></a>
        <ul id="main-menu">
            <li><a href="index.php">Об нас</a></li>
            <li><a href="new.php">Вступить</a></li>
            <li><a href="list.php">Команда</a></li>
        </ul>
        <ul id="user-menu">
            <li><a href="#">Войти</a></li>
            <li><a href="#">Регистрация</a></li>
        </ul>
        <form id="form-search" name="form-search" method="get" action="search.php">
            <input type="text" name="search-text" id="search-text">
            <input type="submit" value="Искать" id="search-submit">
        </form>
    </header>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 header">
                    <h1>Новый член нашей команды: <small>Приветствуем...</small></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-3 col-md-offset-1 avatar">
                    <h4>Аватар</h4>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <section id="fsect1">
            <h3>Об нас</h3>
            <p>Мы вольные каменщики</br>Строим новый Мир.</p>
            <ul id="social-menu">
                <li><a href="#">Fb</a></li>
                <li><a href="#">Ln</a></li>
                <li><a href="#">Tw</a></li>
            </ul>
        </section>
        <section id="fsect2">
            <h3>Наши сервисы</h3>
            <ul class="footer-menu">
                <li><a href="#">Цветные революции</a></li>
                <li><a href="#">Уборка мусора</a></li>
            </ul>
        </section>
        <section id="fsect3">
            <h3>Партнёры</h3>
            <ul class="footer-menu">
                <li><a href="#">Шурик</a></li>
                <li><a href="#">Юрик</a></li>
                <li><a href="#">Лёлик</a></li>
            </ul>
        </section>
        <section id="fsect4">
            <h3>Подпишитесь</h3>
            <form id="form-subscribe" name="form-subscribe" method="get" action="thanks.php">
                <input type="email" name="e-mail" id="e-mail" value="Ваш адрес">
                <input type="submit" value="Go" id="SubscrOk">
            </form>
            <p>© SmartNET 2016</p>
        </section>
    </footer>
</div>
</body>
</html>